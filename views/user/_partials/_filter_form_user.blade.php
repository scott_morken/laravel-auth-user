<form method="get" id="filter-form">
    <div class="form-row mb-2">
        <div class="form-group col">
            <label for="first_name" class="sr-only">First Name</label>
            <input type="text" class="form-control" name="first_name" value="{{ $filter->first_name }}"
                   placeholder="First">
        </div>
        <div class="form-group col">
            <label for="last_name" class="sr-only">Last Name</label>
            <input type="text" class="form-control" name="last_name" value="{{ $filter->last_name }}"
                   placeholder="Last">
        </div>
        <div class="col">
            <button type="submit" class="btn btn-primary">Filter</button>
            <a href="{{ action($controller . '@getIndex') }}" title="Clear Filter" class="btn btn-outline-warning">Clear
                Filter</a>
        </div>
    </div>
</form>
