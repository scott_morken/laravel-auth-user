@extends($master)

@section('content')
    <h4>Update user #{{ $model->id }}</h4>
    <div class="alert alert-info">
        User information is retrieved from LDAP. No changes can be made.
    </div>
    {!!
    HTML::detailView(
        $model,
        [
            'id' => 'ID',
            'username' => 'Username',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'email' => 'Email',
        ]
    )
    !!}
    <?php $allowed_roles = config('smorken/rbac::config.allowed_roles', ['admin']); ?>
    <?php
    if (rbac()->isSuperAdmin()) :
        array_unshift($allowed_roles, 'super-admin');
    endif;
    ?>
    <form method="post" action="{{ action($controller . '@postUpdate', ['id' => $model->id]) }}">
        {{ csrf_field() }}
        <div class="body">
            @foreach(rbac()->all() as $role)
                @if (in_array($role->role_name, $allowed_roles))
                    <div class="form-check">
                        <label for="role-{{ $role->id }}" class="form-check-label">
                            <input type="hidden" name="role[{{ $role->id }}]" value="0">
                            <input type="checkbox" name="role[{{ $role->id }}]" value="1" class="form-check-input"
                                   id="role-{{ $role->id }}" {{ $model->hasRole($role->id, false) ? 'checked' : null }}>
                            {{ $role->description }}
                        </label>
                    </div>
                @endif
            @endforeach
        </div>
        <div>
            <button type="submit" class="btn btn-default">Save</button>
            <a href="{{ action($controller . '@getIndex') }}" title="Cancel" class="btn btn-danger">Cancel</a>
        </div>
    </form>
@stop
