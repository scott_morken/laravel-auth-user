@extends($master)
@section('content')
    <h4>Users</h4>
    @include('smorken/auth::user._partials._filter_form_user')
    {!! HTML::tableView(
    $models,
    array(
    'id' => 'ID',
    'username' => 'Username',
    'first_name' => 'First Name',
    'last_name' => 'Last Name',
    'email' => 'Email',
    'impersonate' => array(
        'label' => 'Impersonate',
        'raw' => true,
        'value' => function($m) use ($controller) {
            $url = action($controller . '@getImpersonate', array('id' => $m->id));
            return '<a href="'. $url . '" title="Impersonate user">Impersonate</a>';
        }
    )
    ),
    ['class' => 'table-striped'],
    $controller
    ) !!}
@stop
