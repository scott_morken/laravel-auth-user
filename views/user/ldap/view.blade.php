@extends($master)

@section('content')
    <h4>User #{{ $model->id }}</h4>
    {!!
    HTML::detailView(
    $model,
    array(
    'id' => 'ID',
    'username' => 'Username',
    'first_name' => 'First Name',
    'last_name' => 'Last Name',
    'email' => 'Email',
    )
    )
    !!}
    <?php $allowed_roles = config('smorken/rbac::config.allowed_roles', ['admin']); ?>
    <?php
    if (rbac()->isSuperAdmin()) :
        array_unshift($allowed_roles, 'super-admin');
    endif;
    ?>
    <div class="body">
        @foreach(rbac()->all() as $role)
            @if (in_array($role->role_name, $allowed_roles))
                <div>
                    @if ($model->hasRole($role->id, false))
                        <span class="glyphicon glyphicon-check fa fa-check-square"></span>
                    @else
                        <span class="glyphicon glyphicon-unchecked fa fa-square"></span>
                    @endif
                    {{ $role->description }}
                </div>
            @endif
        @endforeach
    </div>
    <div class="clearfix"></div>
@stop
