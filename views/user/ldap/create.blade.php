@extends($master)
@section('content')
    <h4>Create new user</h4>
    @include('smorken/auth::user.ldap._search')
    <h5>Search results</h5>
    {!! HTML::tableView(
        $results,
        array(
            'id' => array(
                'label' => 'ID',
                'raw' => true,
                'value' => function($m) use ($controller) {
                    return link_to_action($controller . '@getSave', $m->id, array('id' => $m->id));
                }
            ),
            'username' => 'Username',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'email' => 'Email',
        ),
        array('class' => 'table-striped')
    ) !!}
@stop
