<form id="filter-form" method="get">
    <div class="form-row mb-2">
        <div class="form-group col">
            <label for="id" class="sr-only">MEID or SID</label>
            <input type="text" name="id" value="{{ $request->id }}" placeholder="MEID or SID" class="form-control"
                   maxlength="20">
        </div>
        <div class="form-group col">
            <label for="first_name" class="sr-only">First Name</label>
            <input type="text" name="first_name" value="{{ $request->first_name }}" placeholder="First"
                   class="form-control" maxlength="64">
        </div>
        <div class="form-group col">
            <label for="last_name" class="sr-only">Last Name</label>
            <input type="text" name="last_name" value="{{ $request->last_name }}" placeholder="Last"
                   class="form-control" maxlength="64">
        </div>
        <div class="col">
            <button type="submit" name="search-button" value="search" class="btn btn-default">Find</button>
        </div>
    </div>
</form>
