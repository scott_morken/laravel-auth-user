<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/15/16
 * Time: 9:56 AM
 */
use Mockery as m;

class UserStorageTest extends \PHPUnit\Framework\TestCase
{

    public function tearDown()
    {
        m::close();
    }

    public function testFilter()
    {
        $query = m::mock('Query');
        $filter = new \Smorken\Storage\Filter\Filter(['first_name' => 'foo', 'last_name' => 'bar']);
        list($sut, $m, $r) = $this->getSutAndMocks();
        $m->shouldReceive('newQuery')->once()->andReturn($query);
        $query->shouldReceive('firstNameLike')->once()->with('foo')->andReturn($query);
        $query->shouldReceive('lastNameLike')->once()->with('bar')->andReturn($query);
        $query->shouldReceive('paginate')->once()->with(20)->andReturn([]);
        $this->assertEquals([], $sut->filtered($filter));
    }

    public function testHasRoleUsesModel()
    {
        list($sut, $m) = $this->getSutAndMocks();
        $m->shouldReceive('hasRole')->once()->with(1, false)->andReturn(true);
        $this->assertTrue($sut->hasRole($m, 1));
    }

    public function testHandleRoleAssignments()
    {
        $roles = [
            1 => false,
            2 => true,
            3 => false,
        ];

        list($sut, $m, $r, $c) = $this->getSutAndMocks();
        $m->id = 99;
        $m->shouldReceive('hasRole')->with('2', false)->andReturn(false);
        $r->shouldReceive('remove')->with(99, 1)->andReturn(true);
        $r->shouldReceive('remove')->with(99, 3)->andReturn(true);
        $r->shouldReceive('add')->with(99, 2)->andReturn(true);
        $c->shouldReceive('flush')->once();
        $sut->handleRoleAssignments($m, $roles);
        $this->assertTrue(true); //todo add assertion
    }

    protected function getSutAndMocks()
    {
        $cache = m::mock('Illuminate\Contracts\Cache\Repository');
        $rbac = m::mock('Smorken\Rbac\Contracts\Storage\RoleUser');
        $model = m::mock('Model');
        $sut = new \Smorken\Auth\User\Storage\Eloquent\User($model);
        $sut->setRbac($rbac);
        $sut->setCache($cache);
        return [$sut, $model, $rbac, $cache];
    }
}
