<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/15/16
 * Time: 10:39 AM
 */

namespace {

    $view_mock = null;
    $session_mock = null;
    $redirect_mock = null;
    $logger_mock = null;
    $action_mock = null;
    $config_mock = null;
}

namespace Smorken\Auth\User\Http\Controllers\Admin\User {

    use Mockery as m;

    function view($viewname = null)
    {
        global $view_mock;
        if (!$view_mock) {
            $view_mock = m::mock('View');
        }
        return $view_mock;
    }

    function session()
    {
        global $session_mock;
        if (!$session_mock) {
            $session_mock = m::mock('Session');
        }
        return $session_mock;
    }

    function redirect()
    {
        global $redirect_mock;
        if (!$redirect_mock) {
            $redirect_mock = m::mock('Redirect');
        }
        return $redirect_mock;
    }

    function logger()
    {
        global $logger_mock;
        if (!$logger_mock) {
            $logger_mock = m::mock('Logger');
        }
        return $logger_mock;
    }

    class UserControllerTest extends \PHPUnit\Framework\TestCase
    {

        public function tearDown()
        {
            m::close();
        }

        public function testGetIndex()
        {
            $v = view();
            list($sut, $a, $u, $r) = $this->getSut();
            $r->shouldReceive('get')->once()->with('last_name', '')->andReturn('');
            $r->shouldReceive('get')->once()->with('first_name', '')->andReturn('');
            $u->shouldReceive('filtered')->once()->andReturn([]);
            $v->shouldReceive('with')->once()->with('models', [])->andReturn($v);
            $v->shouldReceive('with')->once()->with('filter', m::any())->andReturn($v);
            $this->assertEquals($v, $sut->getIndex($r));
        }

        public function testGetImpersonateIsValid()
        {
            $model = m::mock('UserModel');
            $model->id = 1;
            list($sut, $a, $u, $r) = $this->getSut();
            $u->shouldReceive('find')->with(1)->andReturn($model);
            $a->shouldReceive('loginUsingId')->once()->with(1)->andReturn(true);
            $a->shouldReceive('user->getAuthIdentifier')->once()->andReturn(1);
            session()->shouldReceive('set')->once()->with('impersonating', 1);
            redirect()->shouldReceive('to')->once()->with('/')->andReturn('redirect');
            $this->assertEquals('redirect', $sut->getImpersonate(1));
        }

        public function testGetImpersonateFails()
        {
            $model = m::mock('UserModel');
            $model->id = 1;
            list($sut, $a, $u, $r) = $this->getSut();
            $u->shouldReceive('find')->with(1)->andReturn($model);
            $a->shouldReceive('loginUsingId')->once()->with(1)->andReturn(true);
            $a->shouldReceive('user->getAuthIdentifier')->once()->andReturn(99);
            session()->shouldReceive('flash')->once()->with('warning', 'Unable to impersonate 1');
            redirect()->shouldReceive('action')
                      ->once()
                      ->with('\Smorken\Auth\User\Http\Controllers\Admin\User\LdapUserController@getIndex')
                      ->andReturn('redirect');
            $this->assertEquals('redirect', $sut->getImpersonate(1));
        }

        public function testGetCreateNotSearching()
        {
            $v = view();
            list($sut, $a, $u, $r) = $this->getSut();
            $r->shouldReceive('get')->once()->with('search-button', false)->andReturn(false);
            $r->shouldReceive('only')->once()->with('id', 'first_name', 'last_name')->andReturn([]);
            $v->shouldReceive('with')->once()->with('results', [])->andReturn($v);
            $v->shouldReceive('with')->once()->with('request', $r)->andReturn($v);
            $this->assertEquals($v, $sut->getCreate($r));
        }

        public function testGetCreateSearchingNumericId()
        {
            $input = ['id' => 12345678, 'first_name' => null, 'last_name' => null];
            $provider = m::mock('AuthProvider');
            $v = view();
            list($sut, $a, $u, $r) = $this->getSut();
            $r->shouldReceive('get')->once()->with('search-button', false)->andReturn(true);
            $r->shouldReceive('only')->once()->with('id', 'first_name', 'last_name')->andReturn($input);
            $a->shouldReceive('getProvider')->once()->andReturn($provider);
            $provider->shouldReceive('find')->once()->with(['id' => 12345678])->andReturn(['results']);
            $v->shouldReceive('with')->once()->with('results', ['results'])->andReturn($v);
            $v->shouldReceive('with')->once()->with('request', $r)->andReturn($v);
            $this->assertEquals($v, $sut->getCreate($r));
        }

        public function testGetCreateSearchingMEIDFirst()
        {
            $input = ['id' => 'AAA1234567', 'first_name' => null, 'last_name' => null];
            $provider = m::mock('AuthProvider');
            $v = view();
            list($sut, $a, $u, $r) = $this->getSut();
            $r->shouldReceive('get')->once()->with('search-button', false)->andReturn(true);
            $r->shouldReceive('only')->once()->with('id', 'first_name', 'last_name')->andReturn($input);
            $a->shouldReceive('getProvider')->once()->andReturn($provider);
            $provider->shouldReceive('find')->once()->with(['username' => 'AAA1234567'])->andReturn(['results']);
            $v->shouldReceive('with')->once()->with('results', ['results'])->andReturn($v);
            $v->shouldReceive('with')->once()->with('request', $r)->andReturn($v);
            $this->assertEquals($v, $sut->getCreate($r));
        }

        public function testGetCreateSearchingMEIDSecond()
        {
            $input = ['id' => 'AAAAA12345', 'first_name' => null, 'last_name' => null];
            $provider = m::mock('AuthProvider');
            $v = view();
            list($sut, $a, $u, $r) = $this->getSut();
            $r->shouldReceive('get')->once()->with('search-button', false)->andReturn(true);
            $r->shouldReceive('only')->once()->with('id', 'first_name', 'last_name')->andReturn($input);
            $a->shouldReceive('getProvider')->once()->andReturn($provider);
            $provider->shouldReceive('find')->once()->with(['username' => 'AAAAA12345'])->andReturn(['results']);
            $v->shouldReceive('with')->once()->with('results', ['results'])->andReturn($v);
            $v->shouldReceive('with')->once()->with('request', $r)->andReturn($v);
            $this->assertEquals($v, $sut->getCreate($r));
        }

        public function testGetCreateSearchingInvalidId()
        {
            $input = ['id' => 1234, 'first_name' => null, 'last_name' => null];
            $provider = m::mock('AuthProvider');
            $v = view();
            list($sut, $a, $u, $r) = $this->getSut();
            $r->shouldReceive('get')->once()->with('search-button', false)->andReturn(true);
            $r->shouldReceive('only')->once()->with('id', 'first_name', 'last_name')->andReturn($input);
            $a->shouldReceive('getProvider')->once()->andReturn($provider);
            $provider->shouldReceive('find')->once()->with([])->andReturn(['results']);
            $v->shouldReceive('with')->once()->with('results', ['results'])->andReturn($v);
            $v->shouldReceive('with')->once()->with('request', $r)->andReturn($v);
            $this->assertEquals($v, $sut->getCreate($r));
        }

        public function testGetCreateSearchingName()
        {
            $input = ['id' => null, 'first_name' => 'foo', 'last_name' => 'bar'];
            $provider = m::mock('AuthProvider');
            $v = view();
            list($sut, $a, $u, $r) = $this->getSut();
            $r->shouldReceive('get')->once()->with('search-button', false)->andReturn(true);
            $r->shouldReceive('only')->once()->with('id', 'first_name', 'last_name')->andReturn($input);
            $a->shouldReceive('getProvider')->once()->andReturn($provider);
            $provider->shouldReceive('find')->once()->with(['first_name' => 'foo', 'last_name' => 'bar'])->andReturn(
                ['results']
            );
            $v->shouldReceive('with')->once()->with('results', ['results'])->andReturn($v);
            $v->shouldReceive('with')->once()->with('request', $r)->andReturn($v);
            $this->assertEquals($v, $sut->getCreate($r));
        }

        public function testGetSaveExistingUser()
        {
            $user = m::mock('UserModel');
            list($sut, $a, $u, $r) = $this->getSut();
            $a->shouldReceive('getProvider')->once();
            $u->shouldReceive('find')->once()->with(1)->andReturn($user);
            session()->shouldReceive('flash')->once()->with('info', 'User [1] already exists.');
            redirect()->shouldReceive('action')
                ->once()
                ->with('\Smorken\Auth\User\Http\Controllers\Admin\User\LdapUserController@getIndex')
                ->andReturn('redirect');
            $this->assertEquals('redirect', $sut->getSave(1));
        }

        public function testGetSaveWithNewUser()
        {
            list($sut, $a, $u, $r) = $this->getSut();
            $provider = m::mock('AuthProvider');
            $a->shouldReceive('getProvider')->once()->andReturn($provider);
            $u->shouldReceive('find')->once()->with(1)->andReturnNull();
            $provider->shouldReceive('retrieveById')->once()->with(1)->andReturn(true);
            session()->shouldReceive('flash')->once()->with('success', 'Saved user [1].');
            redirect()->shouldReceive('action')
                      ->once()
                      ->with('\Smorken\Auth\User\Http\Controllers\Admin\User\LdapUserController@getIndex')
                      ->andReturn('redirect');
            $this->assertEquals('redirect', $sut->getSave(1));
        }

        public function testGetSaveWithException()
        {
            list($sut, $a, $u, $r) = $this->getSut();
            $provider = m::mock('AuthProvider');
            $a->shouldReceive('getProvider')->once()->andReturn($provider);
            $u->shouldReceive('find')->once()->with(1)->andReturnNull();
            $e = new \Exception('Foo');
            $provider->shouldReceive('retrieveById')->once()->with(1)->andThrow($e);
            logger()->shouldReceive('error')->once()->with($e);
            session()->shouldReceive('flash')->once()->with('warning', 'Unable to save user [1].');
            redirect()->shouldReceive('action')
                      ->once()
                      ->with('\Smorken\Auth\User\Http\Controllers\Admin\User\LdapUserController@getIndex')
                      ->andReturn('redirect');
            $this->assertEquals('redirect', $sut->getSave(1));
        }

        public function testPostUpdateNoErrors()
        {
            $user = m::mock('UserModel');
            list($sut, $a, $u, $r) = $this->getSut();
            $u->shouldReceive('find')->once()->with(1)->andReturn($user);
            $r->shouldReceive('get')->once()->with('role', [])->andReturn([1 => true]);
            $u->shouldReceive('handleRoleAssignments')->once()->with($user, [1 => true]);
            $u->shouldReceive('setModel')->once()->with($user);
            $u->shouldReceive('errors')->once()->andReturn(false);
            $u->shouldReceive('name')->once()->with($user)->andReturn('foo');
            session()->shouldReceive('flash')->once()->with('success', 'foo saved.');
            redirect()->shouldReceive('action')
                      ->once()
                      ->with('\Smorken\Auth\User\Http\Controllers\Admin\User\LdapUserController@getIndex')
                      ->andReturn('redirect');
            $this->assertEquals('redirect', $sut->postUpdate($r, 1));
        }

        public function testPostUpdateHasErrors()
        {
            $user = m::mock('UserModel');
            list($sut, $a, $u, $r) = $this->getSut();
            $u->shouldReceive('find')->once()->with(1)->andReturn($user);
            $r->shouldReceive('get')->once()->with('role', [])->andReturn([1 => true]);
            $u->shouldReceive('handleRoleAssignments')->once()->with($user, [1 => true]);
            $u->shouldReceive('setModel')->once()->with($user);
            $u->shouldReceive('errors')->twice()->andReturn(['error' => 'foo']);
            $u->shouldReceive('id')->once()->with($user)->andReturn(1);
            redirect()->shouldReceive('action')
                      ->once()
                      ->with('\Smorken\Auth\User\Http\Controllers\Admin\User\LdapUserController@getUpdate', ['id' => 1])
                      ->andReturn(redirect());
            redirect()->shouldReceive('withInput')->once()->andReturn(redirect());
            redirect()->shouldReceive('withErrors')->once()->with(['error' => 'foo'])->andReturn('redirect');
            $this->assertEquals('redirect', $sut->postUpdate($r, 1));
        }

        protected function getSut()
        {
            $auth = m::mock('Illuminate\Contracts\Auth\Guard');
            $user = m::mock('Smorken\Auth\User\Contracts\Storage\User');
            $request = m::mock('Illuminate\Http\Request');
            $ctv = \Smorken\ControllerTraited\view();
            $ctv->shouldReceive('share');
            $sut = new LdapUserController($auth, $user);
            return [$sut, $auth, $user, $request];
        }
    }
}

namespace Smorken\ControllerTraited {

    use Mockery as m;

    function view($viewname = null)
    {
        global $view_mock;
        if (!$view_mock) {
            $view_mock = m::mock('View');
        }
        return $view_mock;
    }

    function action()
    {
        global $action_mock;
        if (!$action_mock) {
            $action_mock = m::mock('Action');
        }
        return $action_mock;
    }

    function config()
    {
        global $config_mock;
        if (!$config_mock) {
            $config_mock = m::mock('Config');
        }
        return $config_mock;
    }
}

namespace Smorken\ControllerTraited\Traits {

    use Mockery as m;

    function action()
    {
        global $action_mock;
        if (!$action_mock) {
            $action_mock = m::mock('Action');
        }
        return $action_mock;
    }

    function config()
    {
        global $config_mock;
        if (!$config_mock) {
            $config_mock = m::mock('Config');
        }
        return $config_mock;
    }
}

namespace Smorken\Storage\Filter {

    use Mockery as m;

    function session()
    {
        global $session_mock;
        if (!$session_mock) {
            $session_mock = m::mock('Session');
        }
        return $session_mock;
    }
}
