<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/15/16
 * Time: 9:16 AM
 */
use Mockery as m;

class UserTest extends \PHPUnit\Framework\TestCase
{

    public function tearDown()
    {
        m::close();
    }

    public function testScopeLastNameLike()
    {
        $query = m::mock('Query');
        $query->shouldReceive('where')->once()->with('last_name', 'LIKE', 'foo%')->andReturn($query);
        $sut = $this->getSut();
        $r = $sut->scopeLastNameLike($query, 'foo');
        $this->assertEquals($query, $r);
    }

    public function testScopeFirstNameLike()
    {
        $query = m::mock('Query');
        $query->shouldReceive('where')->once()->with('first_name', 'LIKE', 'foo%')->andReturn($query);
        $sut = new \Smorken\Auth\User\Models\Eloquent\User();
        $r = $sut->scopeFirstNameLike($query, 'foo');
        $this->assertEquals($query, $r);
    }

    public function testName()
    {
        $sut = $this->getSut(['first_name' => 'foo', 'last_name' => 'bar']);
        $this->assertEquals('foo bar', $sut->name());
    }

    public function testShortName()
    {
        $sut = $this->getSut(['first_name' => 'foo', 'last_name' => 'bar']);
        $this->assertEquals('f. bar', $sut->shortName());
    }

    public function testShortNameNoFirstName()
    {
        $sut = $this->getSut(['first_name' => '', 'last_name' => 'bar']);
        $this->assertEquals(' bar', $sut->shortName());
    }

    public function testGetLogin()
    {
        $sut = $this->getSut(['username' => 'foobar']);
        $this->assertEquals('foobar', $sut->getLogin());
    }

    public function testGetLoginWithNewLoginField()
    {
        \Smorken\Auth\User\Models\Eloquent\User::setLoginField('first_name');
        $sut = $this->getSut(['username' => 'foobar', 'first_name' => 'fuzz']);
        $this->assertEquals('fuzz', $sut->getLogin());
    }

    public function testHasRoleWithoutIdIsFalse()
    {
        $rbac = m::mock('Rbac');
        $rbac->shouldReceive('hasRole')->with(1, true, null)->andReturn(false);
        $sut = $this->getSut();
        $sut->setRbacProvider($rbac);
        $this->assertFalse($sut->hasRole(1));
    }

    public function testHasRoleLoadsRolesFromRbac()
    {
        $rbac = m::mock('Rbac');
        $sut = $this->getSut(['id' => 2]);
        $rbac->shouldReceive('hasRole')->with(1, true, 2)->andReturn(false);
        $sut->setRbacProvider($rbac);
        $this->assertFalse($sut->hasRole(1));
    }

    public function testHasRoleLoadsRolesFromRbacAndMatches()
    {
        $rbac = m::mock('Rbac');
        $rbac->shouldReceive('hasRole')->with(1, true, 2)->andReturn(true);
        $sut = $this->getSut(['id' => 2]);
        $sut->setRbacProvider($rbac);
        $this->assertTrue($sut->hasRole(1));
    }

    protected function getSut(array $attributes = [])
    {
        return new \Smorken\Auth\User\Models\Eloquent\User($attributes);
    }
}
