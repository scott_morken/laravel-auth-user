<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/15/16
 * Time: 10:17 AM
 */

namespace Smorken\Auth\User\Models\Traits {

    use Illuminate\Validation\ValidationException;
    use Mockery as m;

    function request()
    {
        $r = m::mock('Request');
        $r->shouldReceive('ajax')->andReturnFalse();
        return $r;
    }

    function redirect()
    {
        $r = m::mock('Symfony\Component\HttpFoundation\Response');
        $r->shouldReceive('to')->with('foo/bar')->andReturn($r);
        $r->shouldReceive('withInput->withErrors')->andReturn($r);
        return $r;
    }

    class ValidateUserTest extends \PHPUnit\Framework\TestCase
    {

        public function tearDown()
        {
            m::close();
        }

        public function testValidatorReturnsNotFailedStatus()
        {
            list($sut, $val) = $this->getSut();
            $val->shouldReceive('make')->once()->with(['first_name' => 'bar'], ['first_name' => 'required'])->andReturn(
                $val
            );
            $val->shouldReceive('validate')->once()->andReturn(null);
            $this->assertTrue($sut->validate(['first_name' => 'bar'], ['first_name' => 'required']));
        }

        public function testValidatorExceptionOnFail()
        {
            $v = m::mock('Illuminate\Contracts\Validation\Validator');
            list($sut, $val, $url) = $this->getSut();
            $val->shouldReceive('make')->once()->with(['first_name' => 'bar'], ['first_name' => 'required'])->andReturn(
                $v
            );
            $e = new ValidationException($v);
            $v->shouldReceive('validate')->andThrow($e);
            $this->expectExceptionMessage('The given data was invalid.');
            $sut->validate(['first_name' => 'bar'], ['first_name' => 'required']);
        }

        protected function getSut()
        {
            $val = m::mock('Illuminate\Contracts\Validation\Factory');
            $url = m::mock('Illuminate\Routing\UrlGenerator');
            $sut = new ModelStub();
            $sut->setValidationFactory($val);
            $sut->setUrlGenerator($url);
            return [$sut, $val, $url];
        }
    }

    class ModelStub
    {

        use ValidateUser;
    }
}
