<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/15/16
 * Time: 9:50 AM
 */
use Mockery as m;

class VOUserTest extends \PHPUnit\Framework\TestCase
{

    public function tearDown()
    {
        m::close();
    }

    public function testFillSetsAttributes()
    {
        $sut = $this->getSut(['foo' => 'bar', 'fiz' => 'buz']);
        $sut->fill(['foo' => 'biz']);
        $this->assertEquals(['foo' => 'biz', 'fiz' => 'buz'], $sut->getAttributes());
    }

    public function testSaveReturnsTrue()
    {
        $sut = $this->getSut();
        $this->assertTrue($sut->save());
    }

    public function testFirstNameLikeReturnsVOObject()
    {
        $sut = $this->getSut();
        $this->assertEquals($sut, $sut->firstNameLike('foo'));
    }

    public function testLastNameLikeReturnsVOObject()
    {
        $sut = $this->getSut();
        $this->assertEquals($sut, $sut->lastNameLike('foo'));
    }

    protected function getSut(array $attributes = [])
    {
        return new \Smorken\Auth\User\Models\VO\User($attributes);
    }
}
