<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('users', function (Blueprint $table) {

            $table->string('id', 32);
            $table->string('username', 32)->unique();
            $table->string('email', 100)->unique();
            $table->string('first_name', 32);
            $table->string('last_name', 32);
            $table->string('password', 64)->nullable();
            $table->string('remember_token', 64)->nullable();
            $table->timestamps();
            $table->primary('id');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
