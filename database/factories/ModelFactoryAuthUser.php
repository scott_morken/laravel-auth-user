<?php
$factory->define(
    Smorken\Auth\User\Models\Eloquent\User::class,
    function (Faker\Generator $faker) {
        return [
            'id'         => $faker->unique()->randomNumber(8),
            'first_name' => $faker->firstName,
            'last_name'  => $faker->lastName,
            'email'      => $faker->safeEmail,
            'username'   => str_random(5) . $faker->randomNumber(5),
        ];
    }
);
