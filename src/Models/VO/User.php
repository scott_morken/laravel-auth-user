<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/11/16
 * Time: 7:32 AM
 */

namespace Smorken\Auth\User\Models\VO;

use Illuminate\Auth\Authenticatable;
use Smorken\Auth\User\Models\Traits\CommonUser;
use Smorken\Auth\User\Models\Traits\Rbac;
use Smorken\Auth\User\Models\Traits\ValidateUser;
use Smorken\Model\VO\Model;

class User extends Model implements \Smorken\Auth\User\Contracts\Models\User
{

    use CommonUser, ValidateUser, Rbac, Authenticatable;

    public function fill(array $attributes)
    {
        $this->setAttributes($attributes);
    }

    public function save(array $options = [])
    {
        return true;
    }

    public function getKey()
    {
        return $this->getAttribute($this->getKeyName());
    }

    public function getKeyName()
    {
        return 'id';
    }

    public function newQuery()
    {
        return $this->newInstance();
    }

    public function where()
    {
        return $this;
    }

    public function first()
    {
        return null;
    }

    public function find($identifier)
    {
        return null;
    }

    /**
     * Placeholder for 'like' since VOs aren't searchable
     * @param $search
     * @return $this
     */
    public function firstNameLike($search)
    {
        return $this;
    }

    /**
     * Placeholder for 'like' since VOs aren't searchable
     * @param $search
     * @return $this
     */
    public function lastNameLike($search)
    {
        return $this;
    }
}
