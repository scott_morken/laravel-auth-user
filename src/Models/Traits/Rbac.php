<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/15/16
 * Time: 7:40 AM
 */

namespace Smorken\Auth\User\Models\Traits;

trait Rbac
{

    protected $rbac_provider;

    public function hasRole($role_id, $inherited = true)
    {
        $provider = $this->getRbacProvider();
        if ($provider) {
            return $provider->hasRole($role_id, $inherited, $this->id);
        }
        return false;
    }

    public function setRbacProvider($provider)
    {
        $this->rbac_provider = $provider;
    }

    public function getRbacProvider()
    {
        if (!$this->rbac_provider) {
            if (function_exists('rbac')) {
                $this->rbac_provider = rbac();
            }
        }
        return $this->rbac_provider;
    }
}
