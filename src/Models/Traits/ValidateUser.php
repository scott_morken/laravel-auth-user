<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/15/16
 * Time: 7:25 AM
 */

namespace Smorken\Auth\User\Models\Traits;

use Illuminate\Contracts\Routing\UrlGenerator;
use Illuminate\Contracts\Validation\Factory;

trait ValidateUser
{

    /**
     * @var \Illuminate\Contracts\Validation\Factory
     */
    protected $validation_factory;

    /**
     * @var \Illuminate\Routing\UrlGenerator
     */
    protected $url_generator;

    public function rules()
    {
        return [
            'id'         => 'required|min:1|max:32',
            'username'   => 'required|alpha_num|min:1|max:32',
            'first_name' => 'required|max:32',
            'last_name'  => 'required|max:32',
            'email'      => 'required|email',
        ];
    }

    /**
     * Validate the given request with the given rules.
     *
     * @param  array $attributes
     * @param  array $rules
     * @return bool
     */
    public function validate($attributes, array $rules)
    {
        if ($this->getValidationFactory()) {
            $this->getValidationFactory()->make($attributes, $rules)->validate();
        }
        return true;
    }

    /**
     * @param $url_generator
     * @return \Illuminate\Routing\UrlGenerator
     */
    public function setUrlGenerator($url_generator)
    {
        $this->url_generator = $url_generator;
    }

    /**
     * @return \Illuminate\Routing\UrlGenerator
     */
    protected function getUrlGenerator()
    {
        if (!$this->url_generator) {
            $this->url_generator = app(UrlGenerator::class);
        }
        return $this->url_generator;
    }

    /**
     * Get the URL we should redirect to.
     *
     * @return string
     */
    protected function getRedirectUrl()
    {
        return $this->getUrlGenerator()->previous();
    }

    /**
     * @param Factory $factory
     */
    public function setValidationFactory(Factory $factory = null)
    {
        $this->validation_factory = $factory;
    }

    /**
     * Get a validation factory instance.
     *
     * @return \Illuminate\Contracts\Validation\Factory
     */
    protected function getValidationFactory()
    {
        if (!$this->validation_factory) {
            $this->validation_factory = app(Factory::class);
        }
        return $this->validation_factory;
    }
}
