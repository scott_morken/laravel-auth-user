<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/15/16
 * Time: 7:55 AM
 */

namespace Smorken\Auth\User\Models\Traits;

trait CommonUser
{

    protected static $loginField = 'username';

    public function name()
    {
        return sprintf('%s %s', $this->first_name, $this->last_name);
    }

    public function shortName()
    {
        $first = $this->first_name ? substr($this->first_name, 0, 1) . '.': '';
        return sprintf('%s %s', $first, $this->last_name);
    }

    public static function setLoginField($value)
    {
        static::$loginField = $value;
    }

    public function getLoginField()
    {
        return static::$loginField;
    }

    public function getLogin()
    {
        return $this->{$this->getLoginField()};
    }

    public function __toString()
    {
        return $this->name();
    }
}
