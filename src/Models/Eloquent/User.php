<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/11/16
 * Time: 7:32 AM
 */

namespace Smorken\Auth\User\Models\Eloquent;

use Illuminate\Auth\Authenticatable;
use Smorken\Auth\User\Models\Traits\CommonUser;
use Smorken\Auth\User\Models\Traits\Rbac;
use Smorken\Auth\User\Models\Traits\ValidateUser;
use Smorken\Storage\Models\Eloquent\Base;

class User extends Base implements \Smorken\Auth\User\Contracts\Models\User
{

    use CommonUser, ValidateUser, Rbac, Authenticatable;

    protected $fillable = [
        'id',
        'username',
        'first_name',
        'last_name',
        'email',
        'password',
        'remember_token',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    public $incrementing = false;

    public function scopeLastNameLike($query, $name)
    {
        return $this->like($query, 'last_name', $name);
    }

    public function scopeFirstNameLike($query, $name)
    {
        return $this->like($query, 'first_name', $name);
    }

    protected function like($query, $column, $name)
    {
        if ($name === '*' || $name === '') {
            return $query;
        }
        $query->where($column, 'LIKE', $name . '%');
        return $query;
    }
}
