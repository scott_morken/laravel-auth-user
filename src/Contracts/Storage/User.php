<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/11/16
 * Time: 7:28 AM
 */

namespace Smorken\Auth\User\Contracts\Storage;

interface User
{

    public function handleRoleAssignments($model, array $roles = []);

    public function hasRole($model, $role_id);
}
