<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/11/16
 * Time: 7:28 AM
 */

namespace Smorken\Auth\User\Contracts\Models;

use Illuminate\Contracts\Auth\Authenticatable;

/**
 * Interface User
 * @package Smorken\Auth\User\Contracts\Models
 *
 * @property string $id 32 chars
 * @property string $username 32 chars, unique
 * @property string $first_name 32 chars
 * @property string $last_name 32 chars
 * @property string $email 100 chars
 * @property string|null $password 64 chars Required for compatibility with Laravel Auth
 * @property string|null $remember_token 64 chars Required for compatibility with Laravel Auth
 */
interface User extends Authenticatable
{

    public function name();

    public function shortName();

    public function validate($attributes, array $rules);

    public function getLoginField();

    public function getLogin();

    public function hasRole($role_id, $inherited = true);

    public function setRbacProvider($provider);

    public function getRbacProvider();

    /**
     * Fill the model with an array of attributes.
     *
     * @param  array  $attributes
     * @return $this
     */
    public function fill(array $attributes);

    public function save(array $options = []);

    public function getKey();

    public function getKeyName();

    public function newQuery();
}
