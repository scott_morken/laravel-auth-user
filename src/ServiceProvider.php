<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/15/16
 * Time: 8:57 AM
 */

namespace Smorken\Auth\User;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{

    public function boot()
    {
        $this->loadViewsFrom(__DIR__ . '/../views', 'smorken/auth');
        $this->app->make('Illuminate\Database\Eloquent\Factory')->load(__DIR__ . '/../database/factories');
        $this->loadMigrationsFrom( __DIR__ . '/../database/migrations');
        $this->publishes(
            [
                __DIR__ . '/../views' => base_path('/resources/views/vendor/smorken/auth'),
            ],
            'views'
        );
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        // nothing to do here, register the storage provider via the normal 'storage' options
    }
}
