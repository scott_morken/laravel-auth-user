<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/11/16
 * Time: 7:35 AM
 */

namespace Smorken\Auth\User\Storage\VO;

use Smorken\Auth\User\Storage\Traits\CommonUser;
use Smorken\Storage\Storage\Base\AbstractArray;

class User extends AbstractArray implements \Smorken\Auth\User\Contracts\Storage\User
{

    use CommonUser;
}
