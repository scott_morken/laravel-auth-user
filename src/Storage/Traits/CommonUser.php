<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/15/16
 * Time: 8:20 AM
 */

namespace Smorken\Auth\User\Storage\Traits;

use Smorken\Rbac\Contracts\Storage\RoleUser;

trait CommonUser
{

    /**
     * @var \Illuminate\Contracts\Cache\Repository
     */
    protected $cache;

    /**
     * @var RoleUser
     */
    protected $rbac;

    protected function getFilterQuery($filter, $criteria)
    {
        $map = [
            'first_name' => 'firstNameLike',
            'last_name'  => 'lastNameLike',
        ];
        $query = $this->make($criteria);
        foreach ($filter->toArray() as $scope => $value) {
            if (isset($map[$scope])) {
                $methodname = $map[$scope];
                $query->$methodname($value);
            }
        }

        return $query;
    }

    public function handleRoleAssignments($model, array $roles = [])
    {
        $role_user = $this->getRbac();
        foreach ($roles as $role_id => $has_role) {
            if (!$has_role) {
                $role_user->remove($model->id, $role_id);
            } else {
                if (!$this->hasRole($model, $role_id)) {
                    $role_user->add($model->id, $role_id);
                }
            }
        }
        $this->getCache()->flush();
    }

    public function hasRole($model, $role_id)
    {
        return $model->hasRole($role_id, false);
    }

    /**
     * @param \Illuminate\Contracts\Cache\Repository $cache
     */
    public function setCache(\Illuminate\Contracts\Cache\Repository $cache)
    {
        $this->cache = $cache;
    }

    /**
     * @return \Illuminate\Contracts\Cache\Repository
     */
    protected function getCache()
    {
        if (!$this->cache) {
            $this->cache = app('Illuminate\Contracts\Cache\Repository');
        }
        return $this->cache;
    }

    public function setRbac(RoleUser $rbac)
    {
        $this->rbac = $rbac;
    }

    /**
     * @return RoleUser
     */
    protected function getRbac()
    {
        if (!$this->rbac) {
            $name = config('smorken/rbac::config.role_user.name');
            if ($name) {
                $this->rbac = app($name);
            }
        }
        return $this->rbac;
    }
}
