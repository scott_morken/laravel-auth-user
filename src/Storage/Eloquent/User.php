<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/11/16
 * Time: 7:35 AM
 */

namespace Smorken\Auth\User\Storage\Eloquent;

use Smorken\Auth\User\Storage\Traits\CommonUser;
use Smorken\Storage\Contracts\Storage\Crud;
use Smorken\Storage\Contracts\Storage\Pageable;
use Smorken\Storage\Storage\Eloquent\Base;

class User extends Base implements \Smorken\Auth\User\Contracts\Storage\User, Crud, Pageable
{

    use CommonUser;
}
