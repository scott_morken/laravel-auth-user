<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/15/16
 * Time: 8:32 AM
 */

namespace Smorken\Auth\User\Http\Controllers\Admin\User;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Smorken\Auth\User\Contracts\Storage\User;
use Smorken\Ext\Controller\BaseController;
use Smorken\Storage\Filter\Filter;

class LdapUserController extends BaseController
{

    use \Smorken\Ext\Controller\Traits\Provider, \Smorken\Ext\Controller\Traits\Ops;

    protected $master = 'smorken/views::layouts.rightops';

    protected $package = 'smorken/auth::';

    protected $base = 'user.ldap';

    protected $subnav = 'admin';

    /**
     * @var Guard
     */
    protected $auth;

    public function __construct(Guard $auth, User $provider)
    {
        $this->auth = $auth;
        $this->setProvider($provider);
        parent::__construct();
    }

    public function getIndex(Request $r)
    {
        list($models, $filter) = $this->userData($r);
        $this->handleOps($r);

        return view($this->getViewName('index'))
            ->with('models', $models)
            ->with('filter', $filter);
    }

    public function getView(Request $r, $id)
    {
        return $this->handleCommonGets($r,'view', true, $id);
    }

    public function getImpersonate($id)
    {
        $user = $this->loadModel($id);
        $this->auth->loginUsingId($id);
        $auth_id = $this->auth->user()->getAuthIdentifier();
        if ($auth_id == $user->id) {
            session()->put('impersonating', $id);
            return redirect()->to('/');
        } else {
            session()->flash('warning', "Unable to impersonate $id");
            return redirect()->action($this->getRoute('getIndex'));
        }
    }

    public function getCreate(Request $r)
    {
        $results = $this->search($r);
        $this->handleOps($r);

        return view($this->getViewName('create'))
            ->with('results', $results)
            ->with('request', $r);
    }

    public function getSave($id)
    {
        $authprovider = $this->auth->getProvider();
        $user = $this->getProvider()->find($id);
        if ($user) {
            session()->flash('info', "User [$id] already exists.");
        } else {
            try {
                $user = $authprovider->retrieveById($id);
                session()->flash('success', "Saved user [$id].");
            } catch (\Exception $e) {
                logger()->error($e);
                session()->flash('warning', "Unable to save user [$id].");
            }
        }

        return redirect()->action($this->getRoute('getIndex'));
    }

    public function getUpdate(Request $r, $id)
    {
        return $this->handleCommonGets($r,'update', true, $id);
    }

    public function postUpdate(Request $r, $id)
    {
        $model = $this->loadModel($id);
        $this->handleRoles($model, $r->get('role', []));
        $this->getProvider()->setModel($model);
        if (!$this->getProvider()->errors()) {
            Cache::flush();
            session()->flash('success', $this->getProvider()->name($model) . ' saved.');
            return redirect()->action($this->getRoute('getIndex'));
        }
        return redirect()->action($this->getRoute('getUpdate'), ['id' => $this->getProvider()->id($model)])
                         ->withInput()
                         ->withErrors($this->getProvider()->errors());
    }

    public function getDelete(Request $r, $id)
    {
        return $this->handleCommonGets($r, 'delete', true, $id);
    }

    public function deleteDelete($id)
    {
        if ($this->getProvider()->delete($this->loadModel($id))) {
            Cache::flush();
            session()->flash('success', "Resource with id #$id deleted.");
        } else {
            session()->flash('danger', $id . ' NOT deleted.');
        }
        return redirect()->action($this->getRoute('getIndex'));
    }

    protected function handleRoles($model, $roles)
    {
        $this->getProvider()->handleRoleAssignments($model, $roles);
    }

    protected function search(Request $r)
    {
        $action = $r->get('search-button', false);
        $params = $r->only('id', 'first_name', 'last_name');
        if ($action && $params) {
            $params = $this->parseSearchParams($params);
            $authprovider = $this->auth->getProvider();

            return $authprovider->find($params);
        }

        return [];
    }

    protected function parseSearchParams($params)
    {
        $newparams = [];
        if ($params['id']) {
            $newparams = $this->parseUserIdentifiers($params['id']);
        }
        if ($params['first_name']) {
            $newparams['first_name'] = $params['first_name'];
        }
        if ($params['last_name']) {
            $newparams['last_name'] = $params['last_name'];
        }

        return $newparams;
    }

    protected function parseUserIdentifiers($value)
    {
        $params = [];
        if (strlen($value) === 10 && preg_match('/^[A-z]{3,5}[0-9]{5,7}$/', $value)) {
            $params['username'] = $value;
        } elseif ((int)$value == $value && strlen($value) === 8) {
            $params['id'] = $value;
        }
        return $params;
    }

    protected function userData(Request $r)
    {
        $filter = new Filter(
            [
                'last_name'  => $r->get('last_name', ''),
                'first_name' => $r->get('first_name', ''),
            ]
        );
        $criteria = [
            'orderBy' => [
                ['last_name'],
                ['first_name'],
            ],
        ];
        $result = [];
        $result[] = $this->getProvider()->filtered($filter, $criteria);
        $result[] = $filter;

        return $result;
    }

    protected function handleCommonGets(Request $r, $action, $idreq = true, $id = null)
    {
        if ($idreq || $id) {
            $model = $this->loadModel($id);
        } else {
            $model = $this->getProvider()->getModel();
        }
        $this->handleOps($r, $id);
        return view($this->getViewName($action, true))
            ->with('model', $model)
            ->with('base', $this->getViewName());
    }

    public static function ops($id = null, Request $request = null)
    {
        $ops = [
            'Create' => ['url' => action(static::getRoute('getCreate'))],
            'List'   => ['url' => action(static::getRoute('getIndex'))],
        ];
        if ($id) {
            $ops = array_merge($ops, static::opsWithId($id, $request));
        }
        return $ops;
    }

    public static function opsWithId($id, Request $request = null, $show_text = true)
    {
        return [
            'View'   => [
                'url'       => action(static::getRoute('getView'), ['id' => $id]),
                'icon'      => config('ext-controller.icons.view', 'fa fa-eye'),
                'show_text' => $show_text,
            ],
            'Update' => [
                'url'       => action(static::getRoute('getUpdate'), ['id' => $id]),
                'icon'      => config('ext-controller.icons.update', 'fa fa-pencil'),
                'show_text' => $show_text,
            ],
            'Delete' => [
                'url'       => action(static::getRoute('getDelete'), ['id' => $id]),
                'icon'      => config('ext-controller.icons.delete', 'fa fa-trash'),
                'show_text' => $show_text,
            ],
        ];
    }
}
